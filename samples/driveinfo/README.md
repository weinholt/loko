<!--
SPDX-FileCopyrightText: 2022 Gwen Weinholt

SPDX-License-Identifier: EUPL-1.2+
-->

# Drive information viewer

This program scans for ATA devices and lists information.
